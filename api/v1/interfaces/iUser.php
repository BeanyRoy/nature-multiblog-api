<?php

    interface IUser {
        public function onGet($userId);
        public function onDelete($userId);
    }

?>