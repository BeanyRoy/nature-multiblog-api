<?php

    require_once '../../objects/entry.php';
    require_once '../../db/mysql_db.php';

    $db = new mysql_db();
    
    if ($db !== null)
    {
        switch ($_SERVER['REQUEST_METHOD'])
        {
            case 'GET': //Darüber nachdenken, ob nicht einfach der User an den Entry gejoint werden soll
                if (isset($_GET['id']))
                {
                    $entry = new Entry();

                    $getEntryStmt = $db->conn->prepare("select f_e_id, f_e_date, f_e_name, f_e_creator, f_e_prologue, f_e_epilogue, f_e_recipe
                                                        from f_entry
                                                        where f_e_id = ?;");
                    $getEntryStmt->bind_param('i', $_GET['id']);
                    $getEntryStmt->execute();
                    $getEntryStmt->bind_result($entryId, $date, $entryName, $creatorId, $prologue, $epilogue, $recipeId);
                    
                    if ($getEntryStmt->fetch())
                    {
                        $entry->id = $entryId;
                        $entry->date = $date;
                        $entry->name = $entryName;
                        $entry->prologue = $prologue;
                        $entry->epilogue = $epilogue;
                        
                        $getEntryCreatorStmt = $db->conn->prepare("select f_u_name
                                                                   from f_user
                                                                   where f_u_id = ?");
                        $getEntryCreatorStmt->bind_param('i', $creatorId);
                        $getEntryCreatorStmt->execute();
                        $getEntryCreatorStmt->bind_result($creatorName);

                        if ($getEntryCreatorStmt->fetch())
                        {
                            $entry->user->id = $creatorId;
                            $entry->user->name = $creatorName;
                        }

                        $getEntryCreatorStmt->close();
                        
                        //Likes nachselektieren und errechnen
                        //Recipe nachselektieren
                        //Comments nachselektieren

                        echo json_encode($entry, JSON_PRETTY_PRINT);
                        http_response_code(200);
                    }
                    else
                    {
                        http_response_code(404);
                    }

                    $getEntryStmt->close();
                }
                break;

            case 'PUT':
                break;
    
            // default:
            //     http_response_code(401);
        }
    }

?>