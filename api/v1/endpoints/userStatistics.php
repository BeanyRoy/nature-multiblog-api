<?php

    require_once '../accessors/userStatisticsContr.php';

    $userStatsContr = new userStatisticsContr();
    $jsonResponse;

    switch ($_SERVER['REQUEST_METHOD'])
    {
        case 'GET':
            $jsonResponse = json_encode($userStatsContr->onGet(), JSON_PRETTY_PRINT);
            break;

        default:
            http_response_code(405);
            return;
    }

    // Auf Fehler prüfen und entsprechenden Statuscode setzen
    echo $jsonResponse;
?>