<?php

    require_once '../accessors/userContr.php';

    $userContr = new UserContr();
    $jsonResponse;

    switch ($_SERVER['REQUEST_METHOD'])
    {
        case 'GET':
            $jsonResponse = json_encode($userContr->onGet($_REQUEST['userId']), JSON_PRETTY_PRINT);
            break;

        default:
            http_response_code(405);
            return;
    }

    // Auf Fehler prüfen und entsprechenden Statuscode setzen
    echo $jsonResponse;

?>