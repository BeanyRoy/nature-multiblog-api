create view v_mb_a_user_stats as
select 
( 
	select count(mb_a_u_id) 
	from mb_a_user
) as user_count,
( 
	select count(mb_a_b_id) 
	from mb_a_blog 
) as blog_count,
( 
	select count(mb_a_b_e_id )
	from mb_a_blog_entry 
) as blog_entry_count
from dual;