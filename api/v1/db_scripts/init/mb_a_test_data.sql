start transaction;

-- clearing of all tables
delete from mb_a_blog_entry;
delete from mb_a_blog;
delete from mb_a_user;

-- mb_a_user
insert into mb_a_user (mb_a_u_surname, mb_a_u_name, mb_a_u_nickname, mb_a_u_email, mb_a_u_password)
values ('Rene', 'Priem', 'BeanyRoy', 'priem', '6B86B273FF34FCE19D6B804EFF5A3F5747ADA4EAA22F1D49C01E52DDB7875B4B');

insert into mb_a_user (mb_a_u_surname, mb_a_u_name, mb_a_u_nickname, mb_a_u_email, mb_a_u_password)
values ('Yasmina', 'Bouziani', 'Mina', '2', 'D4735E3A265E16EEE03F59718B9B5D03019C07D8B6C51F90DA3A666EEC13AB35');

-- mb_a_profile
insert into mb_a_profile (mb_a_p_owner_descr)
values ('This is a sample description of a fictional person who started to blog all his every day doings');

-- mb_a_acconut

-- mb_a_blog
insert into mb_a_blog (mb_a_b_user)
values (1);

-- mb_a_blog_entry
insert into mb_a_blog_entry (mb_a_b_e_headline, mb_a_b_e_parent)
values ('Plastikfreie Waschseife', 1);

insert into mb_a_blog_entry (mb_a_b_e_headline, mb_a_b_e_parent)
values ('Noch ein Thema', 1);

insert into mb_a_blog_entry (mb_a_b_e_headline, mb_a_b_e_parent)
values ('Ein weiteres Thema', 1);

commit;