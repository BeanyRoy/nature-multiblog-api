start transaction;

set sql_safe_updates = 0;

drop schema if exists multiblog_api;
create schema if not exists multiblog_api;

use multiblog_api;

-- The user is used to authenticate the user attempting to log in
create table mb_a_user(
	mb_a_u_id int(10) primary key not null auto_increment,
    mb_a_u_surname varchar(20) not null,
    mb_a_u_name varchar(30) not null,
    mb_a_u_nickname varchar(16) not null,
    mb_a_u_email varchar(30) not null,
    mb_a_u_password varchar(64) not null, /*müssen VOR dem Übertragen gehasht werden (SHA256)*/
    mb_a_u_registration datetime not null default current_timestamp
);

-- profile data
create table mb_a_profile(
	mb_a_p_id int(10) primary key not null auto_increment,
    mb_a_p_owner_descr varchar(2000) not null
    -- not finished yet, evaluate profile properties
);

-- account (composition of user and profile data)
create table mb_a_account(
	mb_a_a_user int(10) not null,
    mb_a_a_profile int(10) not null,
    constraint fk_account_user__user_u_id foreign key (mb_a_a_user) references mb_a_user (mb_a_u_id),
    constraint fk_account_profile__profile_p_id foreign key (mb_a_a_profile) references mb_a_profile (mb_a_p_id),
	constraint uk_user_profile unique (mb_a_a_user, mb_a_a_profile)
);

create table mb_a_blog(
	mb_a_b_id int(10) primary key not null auto_increment,
    mb_a_b_user int(10) not null,
    mb_a_b_created datetime not null default current_timestamp,
    constraint fk_blog_id_user_id foreign key (mb_a_b_user) references mb_a_user(mb_a_u_id)
);

create table mb_a_blog_entry(
	mb_a_b_e_id int(10) primary key not null auto_increment,
    mb_a_b_e_headline varchar(50) not null,
    /*Untertitel*/
    mb_a_b_e_created datetime not null default current_timestamp,
    /*Typ des Eintrages, Text usw...*/
    mb_a_b_e_parent int(10) not null, /*parent is the blog this entry belongs to*/
    constraint fk_blog_entry_parent_id_blog_id foreign key (mb_a_b_e_parent) references mb_a_blog (mb_a_b_id)
);

commit;