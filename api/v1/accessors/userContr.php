<?php

    require_once '../accessors/base/mysqlDB.php';
    require_once 'base/baseResponse.php';
    require_once '../interfaces/iUser.php';
    require_once '../objects/User.php';

    class UserContr implements IUser {
        private $result;
        private $db;

        public function __construct() {
            $this->db = new MysqlDB();
            $this->result = new BaseResponse();
            $this->result->messages = array();
        }

        public function onGet($userId) {
            $this->db->connect();
            $this->result->data = new User();

            $stmt = file_get_contents('../db_scripts/user_get.sql');
            $getUser = $this->db->conn->prepare($stmt);
            $getUser->bind_param('i', $userId);

            try {
                $getUser->execute();
                $getUser->bind_result($surname, $name, $nickname, $registrationDate);

                if ($getUser->fetch()) {
                    $this->result->data->id = $userId;
                    $this->result->data->surname = $surname;
                    $this->result->data->name = $name;
                    $this->result->data->nickname = $nickname;
                    $this->result->data->registrationDate = $registrationDate;
                } else {
                    array_push($this->result->messages, 'api/v1/accessors/userContr.php | User '.$userId.' does not exist');
                }
            } catch (Exception $e) {
                array_push($this->result->messages, 'api/v1/accessors/userContr.php | '.$e->getMessage());
            }

            $getUser->close();
            $this->db->disconnect();

            return $this->result->beautify();
        }

        public function onDelete($userId) {

        }
    }

?>