<?php

    class BaseResponse {
        public $success;
        public $messages;
        public $data;

        public function __construct() {
        }

        /**
         * Summarized functions to clean up a response object
         */
        public function beautify() {
            $this->check_for_errors();
            return array_filter((array) $this, function ($val) {
                return !is_null($val);
            });
        }

        /**
         * Determines the success status of a response object
         */
        private function check_for_errors() {
            if ($this->messages === null || count($this->messages) === 0) {
                $this->messages = null;
                $this->success = true;
            } else {
                $this->data = null;
                $this->success = false;
            }
        }
    }

?>