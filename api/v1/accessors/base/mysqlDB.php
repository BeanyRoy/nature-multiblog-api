<?php

    class MysqlDB {
        private $server = "localhost:3306";
        private $user = "root";
        private $pw = "";
        private $db = "multiblog_api";
        
        public $conn;

        public function __construct() { 
        }

        public function connect() {
            $this->conn = new mysqli($this->server, $this->user, $this->pw, $this->db);
        }

        public function disconnect() { 
            $this->conn->close();
        }
    }

?>