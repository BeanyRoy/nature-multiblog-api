<?php

    require_once '../accessors/base/mysqlDB.php';
    require_once 'base/baseResponse.php';
    require_once '../interfaces/iUserStatistics.php';
    require_once '../objects/userStatistics.php';

    class UserStatisticsContr implements IUserStatistics {
        private $result;
        private $db;

        public function __construct() { 
            $this->db = new MysqlDB();
            $this->result = new BaseResponse();
            $this->result->messages = array();
        }

        public function onGet() {
            $this->db->connect();
            $this->result->data = new UserStatistics();

            $stmt = file_get_contents('../db_scripts/user_statistics_get.sql');
            $getUserStats = $this->db->conn->prepare($stmt);

            try {
                $getUserStats->execute();
                $getUserStats->bind_result($userCount, $blogCount, $entryCount);

                if ($getUserStats->fetch()) {
                    $this->result->data->userCount = $userCount;
                    $this->result->data->blogCount = $blogCount;
                    $this->result->data->entryCount = $entryCount;
                } else {
                    array_push($this->result->messages, 'api/v1/accessors/userStatisticsContr.php | Couldn\'t fetch user count');
                }
            } catch (Exception $e) {
                array_push($result->messages, 'api/v1/accessors/userStatisticsContr.php | '.$e->getMessage());
            }

            $getUserStats->close();
            $this->db->disconnect();

            return $this->result->beautify();
        }
    }  

?>