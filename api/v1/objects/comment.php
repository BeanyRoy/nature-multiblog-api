<?php

    require_once 'user.php';

    class Comment
    {
        public $id;
        public $text;
        public $date;
        public $creator;
        public $answers; //Array of answer

        function __construct() 
        { 
            $this->creator = new User();
            $this->answers = array();
        }
    }

?>