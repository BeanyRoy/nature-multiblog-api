<?php

    require_once 'amount.php';

    class Ingredient
    {
        public $id;
        public $name;
        public $amount;

        function __construct() 
        { 
            $this->amount = new Amount();
        }
    }

?>