<?php

    class Recipe
    {
        public $id;
        public $name;
        public $date;
        public $ingredients;
        public $worksteps;

        function __construct() 
        {
            $this->ingredients = array();
            $this->worksteps = array();
        }
    }

?>