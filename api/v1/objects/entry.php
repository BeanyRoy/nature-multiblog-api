<?php

    require_once 'user.php';
    require_once 'recipe.php';

    class Entry
    {
        public $id;
        public $date;
        public $name;
        public $creator;
        public $likes;
        public $prologue;
        public $epilogue;
        public $recipe;
        public $comments; 
        
        function __construct() 
        { 
            $this->user = new User();
            $this->recipe = new Recipe();
            $this->comments = array();
        }
    }

?>