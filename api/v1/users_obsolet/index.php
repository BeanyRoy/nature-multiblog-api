<?php

    session_start();

    require_once '../../objects/user.php';
    require_once '../../db/mysql_db.php';

    $db = new mysql_db();

    // TEST!!!!!!!!!!!!!!!!!!!!!
    // echo parse_url($_SERVER[]);
    
    // foreach (array_keys($_REQUEST) as $key)
    // {
    //     echo "[".$key."]: ".$_REQUEST[$key]."<br><br>";
    // }
    // TEST!!!!!!!!!!!!!!!!!!!!!

    if ($db != null)
    {
        switch ($_SERVER['REQUEST_METHOD'])
        {
            case 'GET':

                if (array_key_exists('email', $_REQUEST) && array_key_exists('pw', $_REQUEST))
                {
                    $user = new User();
                    $user->email = $_REQUEST['email'];

                    $pw = hash('sha256', $_REQUEST['pw']);

                    $getUserStmt = $db->conn->prepare("select mb_a_u_id, mb_a_u_surname, mb_a_u_name, mb_a_u_nickname, mb_a_u_registration
                                                       from mb_a_user
                                                       where mb_a_u_email = ?
                                                        and mb_a_u_password = ?;");
                    
                    $getUserStmt->bind_param('ss', $user->email, $pw);
                    $getUserStmt->execute();
                    $getUserStmt->bind_result($id, $surname, $name, $nickname, $registration);

                    if ($getUserStmt->fetch())
                    {
                        $user->id = $id;
                        $user->surname = $surname;
                        $user->name = $name; 
                        $user->nickname = $nickname;
                        $user->registrationDate = $registration;
                        
                        http_response_code(200);
                        header('Content-Type: Application/Json');
                        echo json_encode($user, JSON_PRETTY_PRINT);
                    }
                    else 
                    { 
                        http_response_code(401);
                        break;
                    }
                }
                
                break;

            case 'PUT':
                break;
                
            case 'POST':
                break;

            // default:
            //     http_response_code(401);
        }
    }

?>